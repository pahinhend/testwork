import Vue from 'vue';
import { ModalPlugin } from 'bootstrap-vue';
import App from './App.vue';
import router from './router';
import store from './store';

import '@/assets/sass/main.sass';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(ModalPlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
