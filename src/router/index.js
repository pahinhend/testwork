import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Invoice',
    component: () => import(/* webpackChunkName: "invoice" */ '@/views/Invoice.vue'),
  },
  {
    path: '/operdays',
    name: 'Operdays',
    component: () => import(/* webpackChunkName: "operdays" */ '@/views/Operdays.vue'),
  },
  {
    path: '/operations',
    name: 'Operations',
    component: () => import(/* webpackChunkName: "operations" */ '@/views/Operations.vue'),
  },
  {
    path: '/*',
    name: 'error404',
    component: () => import(/* webpackChunkName: "error404" */ '@/views/404.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
