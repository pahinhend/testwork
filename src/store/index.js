import Vue from 'vue';
import Vuex from 'vuex';
import Counters from './counter';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Counters,
  },
});
