import { OpDate } from '@/assets/json/opdate.json';
import { Doc } from '@/assets/json/doc.json';
import { AcctAcct } from '@/assets/json/acct.json';

export default {
  state: {
    dates: [],
    allDates: [],
    leftTable: [],
    rightTable: [],
  },
  getters: {
    dates: (state) => state.dates,
    allDates: (state) => state.allDates,
    selectedDate: (state) => state.selectedDate,
    leftTable: (state) => state.leftTable,
    rightTable: (state) => state.rightTable,
  },
  mutations: {
    setDates(state, dates) {
      state.dates = dates;
    },
    setAllDates(state, dates) {
      state.allDates = dates;
    },
    setLeftTable(state, data) {
      state.leftTable = data;
    },
    setRightTable(state, data) {
      state.rightTable = data;
    },
  },
  actions: {
    getDates({ commit }) {
      const dates = OpDate.sort((a, b) => new Date(b.OpDate) - new Date(a.OpDate));
      commit('setDates', dates.map((date) => date.OpDate));
      commit('setAllDates', dates);
    },
    getAcctByDate({ commit, dispatch, state }, date) {
      dispatch('getDates');
      const selectDate = !date ? state.dates[0] : date;
      commit('setLeftTable', AcctAcct.filter((acct) => acct.OpDate === selectDate));
    },
    getDocsByDate({ commit }, date) {
      const docs = date ? Doc.filter((doc) => doc.OpDate === date[0]) : [];
      commit('setRightTable', docs);
    },
    getDocsByAcct({ commit }, acct) {
      const docs = acct
        ? Doc.filter((doc) => doc.AcctCr === acct[0] || doc.AcctDB === acct[0])
        : [];
      commit('setRightTable', docs);
    },
    getAcctByDoc({ commit }, doc) {
      const act = AcctAcct.filter((acct) => acct.Acct === doc[0] || acct.Acct === doc[1]);
      commit('setRightTable', act);
    },
    getDocs({ commit }) {
      commit('setLeftTable', Doc);
    },
    addLeftTable({ commit, state }, data) {
      commit('setLeftTable', [...data, ...state.leftTable]);
    },
    addRightTable({ commit, state }, data) {
      commit('setRightTable', [...data, ...state.rightTable]);
    },
    setLeftTable({ commit }, data) {
      commit('setLeftTable', data);
    },
    setRightTable({ commit }, data) {
      commit('setRightTable', data);
    },
    getOperationDates({ commit, dispatch, state }) {
      dispatch('getDates');
      commit('setLeftTable', state.allDates);
    },
    clear({ commit }) {
      commit('setLeftTable', []);
      commit('setRightTable', []);
    },
  },
};
