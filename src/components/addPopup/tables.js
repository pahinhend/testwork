export default {
  acct: [
    {
      label: 'номер счета',
      key: 'Acct',
      type: 'number',
    },
    {
      label: 'Остаток',
      key: 'Ost',
      type: 'number',
    },
    {
      label: 'Дата операционного дня',
      key: 'OpDate',
      type: 'text',
      readonly: true,
    },
  ],
  docs: [
    {
      label: 'Счет кредита',
      key: 'AcctCr',
      type: 'number',
    },
    {
      label: 'Счет дебета',
      key: 'AcctDB',
      type: 'number',
    },
    {
      label: 'Сумма',
      key: 'Amount',
      type: 'number',
    },
    {
      label: 'Дата операционного дня',
      key: 'OpDate',
      type: 'text',
    },
  ],
  date: [
    {
      label: 'Дата операционного дня',
      key: 'OpDate',
      type: 'text',
    },
  ],
};
